// cargo test -- --nocapture
use rpi_interface;
use std::thread;
use std::time::Duration;

#[test]
fn test_toggle_relay_pin_01() {
    let pin_number = 11;
    let delay = 1000;
    let result = rpi_interface::rpi_interface::toggle_relay_pin_on(pin_number);
    assert!(result.is_ok());

    thread::sleep(Duration::from_millis(delay));
    
    let result = rpi_interface::rpi_interface::toggle_relay_pin_off(pin_number);
    assert!(result.is_ok());


    thread::sleep(Duration::from_millis(delay));

    let pin_number = 13;
    let delay = 1000;
    let result = rpi_interface::rpi_interface::toggle_relay_pin_on(pin_number);
    assert!(result.is_ok());

    thread::sleep(Duration::from_millis(delay));
    
    let result = rpi_interface::rpi_interface::toggle_relay_pin_off(pin_number);
    assert!(result.is_ok());
}

