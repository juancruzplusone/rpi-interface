use rppal::gpio::{Gpio, InputPin};
use std::error::Error;
// Read state of pin and print to console if changes

fn main() -> Result<(), Box<dyn Error>> {
    println!("Starting test");

    let mut current_pin_state: bool = read_pin_state(40)?;
    let mut new_pin_state = current_pin_state;

    println!("Current pin state: {}", current_pin_state);

    loop {
        if new_pin_state != current_pin_state {
            println!("Pin state changed to: {}", new_pin_state);
            current_pin_state = new_pin_state;
        }

        new_pin_state = read_pin_state(40)?;
    }

}

fn read_pin_state(pin_number : u8) -> Result<(bool), Box<dyn Error>> {
    // Get pin and set to input
    let bcm_pin = map_pin(pin_number)?;
    let mut pin = Gpio::new()?.get(bcm_pin)?.into_input();


    InputPin::set_reset_on_drop(&mut pin, false);

    // read and return current state of pin
    let mut state = pin.is_high();

    Ok(state)
}

fn map_pin(pin: u8) -> Result<u8, String> {
    Ok(match pin {
        3 => 2,
        5 => 3,
        7 => 4,
        8 => 14,
        10 => 15,
        11 => 17,
        12 => 18,
        13 => 27,
        15 => 22,
        16 => 23,
        18 => 24,
        19 => 10,
        21 => 9,
        22 => 25,
        23 => 11,
        24 => 8,
        26 => 7,
        27 => 0,
        28 => 1,
        29 => 5,
        31 => 6,
        32 => 12,
        33 => 13,
        35 => 19,
        36 => 16,
        37 => 26,
        38 => 20,
        40 => 21,
        _ => return Err("Unmapped pin number".to_string()),
    })
}

