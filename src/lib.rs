pub mod rpi_interface {
    use rppal::gpio::{Gpio, OutputPin, InputPin};
    use std::error::Error;
    use std::thread;
    use std::time::Duration;
    
    pub fn toggle_relay_pin_on(relay_pin_number: u8) -> Result<(), Box<dyn Error>> {
        let bcm_pin = map_pin(relay_pin_number)?; 
        let mut pin = Gpio::new()?.get(bcm_pin)?.into_output();

        // Must do below to prevent pin from being reset to low on drop
        // Also pass a mutable reference to the pin as it's being modified
        OutputPin::set_reset_on_drop(&mut pin, false);

        pin.set_high();
        Ok(())
    }

    pub fn toggle_relay_pin_off(relay_pin_number: u8) -> Result<(), Box<dyn Error>> {
        let bcm_pin = map_pin(relay_pin_number)?; 
        let mut pin = Gpio::new()?.get(bcm_pin)?.into_output();

        // Must do below to prevent pin from being reset to low on drop
        // Also pass a mutable reference to the pin as it's being modified
        OutputPin::set_reset_on_drop(&mut pin, false);
        

        pin.set_low();
        Ok(())
    }

    pub fn toggle_relay_pin_on_with_after_delay(relay_pin_number: u8, delay: u64) -> Result<(), Box<dyn Error>> {
        // Toggles pin on and off with a delay afterwards
        let bcm_pin = map_pin(relay_pin_number)?; 
        let mut pin = Gpio::new()?.get(bcm_pin)?.into_output();

        // Must do below to prevent pin from being reset to low on drop
        // Also pass a mutable reference to the pin as it's being modified
        OutputPin::set_reset_on_drop(&mut pin, false);

        pin.set_high();

        // Sleep for delay time
        thread::sleep(Duration::from_millis(delay));
        Ok(())
    }

    pub fn toggle_relay_pin_off_with_after_delay(relay_pin_number: u8, delay: u64) -> Result<(), Box<dyn Error>> {
        // Toggles pin on and off with a delay afterwards
        let bcm_pin = map_pin(relay_pin_number)?; 
        let mut pin = Gpio::new()?.get(bcm_pin)?.into_output();

        // Must do below to prevent pin from being reset to low on drop
        // Also pass a mutable reference to the pin as it's being modified
        OutputPin::set_reset_on_drop(&mut pin, false);

        pin.set_low();

        // Sleep for delay time
        thread::sleep(Duration::from_millis(delay));
        Ok(())
    }

    pub fn get_pin_state(pin_number : u8) -> Result<bool, Box<dyn Error>> {
        // 1 - Pin is high
        // 0 - Pin is low
        // Get pin and set to input
        let bcm_pin = map_pin(pin_number)?;
        let mut pin = Gpio::new()?.get(bcm_pin)?.into_input();
        InputPin::set_reset_on_drop(&mut pin, true);


        // read and return current state of pin
        let state = pin.is_high();

        Ok(state)
    }

    fn map_pin(pin: u8) -> Result<u8, String> {
        Ok(match pin {
            3 => 2,
            5 => 3,
            7 => 4,
            8 => 14,
            10 => 15,
            11 => 17,
            12 => 18,
            13 => 27,
            15 => 22,
            16 => 23,
            18 => 24,
            19 => 10,
            21 => 9,
            22 => 25,
            23 => 11,
            24 => 8,
            26 => 7,
            27 => 0,
            28 => 1,
            29 => 5,
            31 => 6,
            32 => 12,
            33 => 13,
            35 => 19,
            36 => 16,
            37 => 26,
            38 => 20,
            40 => 21,
            _ => return Err("Unmapped pin number".to_string()),
        })
    }
}